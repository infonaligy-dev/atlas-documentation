#  Claims – Lookup Claim By Claim Number
  
##  Step 1
  
Select "Claims Status Lookup" from the menu under Claims
  
![](images/Claims/lookup-a-claim/lookup-claim-by-claim-number.png )
  
##  Step 2
  
###  2.1
  
Make sure you have the "Claim Number" option selected (1).
  
###  2.2
  
Enter the claim number into the field. The claim number is a 5-digit number (2).
  
###  2.3
  
After you enter the claim number, click "Lookup Claim" button to lookup the claim (3).
  
![](images/Claims/lookup-a-claim/lookup-claim.png )
  
##  Step 3
  
View the results of your claim lookup. You are done!
  
![](images/Claims/lookup-a-claim/lookup-claim-by-claim-number-result.png )
  