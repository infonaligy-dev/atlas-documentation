#  Claims – File A Claim
  
##  Step 1
  
Select "File a Claim" from the menu under Claims
  
![](images/Claims/file-a-claim/file-a-claim-navigation.png )
  
##  Step 2
  
###  2.1
  
Select the company (1).
  
###  2.2
  
Select the policy prefix. (2).
  
###  2.3
  
Enter the policy number. (3)
  
###  2.4
  
Enter the loss date. (4)

###  2.5
  
After the fields have been filled. Click "Create Loss Form" to begin filing your claim. (5)
  
![](images/Claims/file-a-claim/file-a-claim-enter-policy-details.png )
  
##  Step 3
Welcome to the new and improved claims form!
### 3.1
  Fill out the details of your claims application (1)
### 3.2
Ensure all fields marked as required are properly filled.  
Required fields are specified in two ways : 
 * The red asterisk symbol right near the field <span style="color:red">(*)</span>.
 * Or a warning that states the field is required  

You will only be able to proceed if all required fields are filled.
### 3.3
After you have finished filling out the form, click the "Submit" button to save your claim. (3)

### 3.4 
Alternatively, you may click the "Cancel" button if you decide to not proceed with the claim. (4)

![](images/Claims/file-a-claim/file-a-claim-fill-in-claim-form-details.png )
  