#  Communications – View All Communications
  
  
##  Step 1

Communications is a useful feature designed to provide an easy and hassle free way to communicate effectively with all users in the application.Note, you can only communicate with users within atlas using communications.It currently supports sms, email and in app messaging.

To view all  communications first click on the "Communications" option under the "Admin" menu.
  
![](images/Communication/create-communication/communication-navigation.png )
  
##  Step 2

Welcome to your communications list page.You will find every communication you create here.
![](images/Communication/view-all-communications/view-all-communications.png )

##  Step 4

### 4.1
You can further filter out your list by searching for keywords specific to your communication or by using the filters provided(1).

### 4.2
Each row of the list has a three-dots button that provides access to actions that can be performed on that row.
You may :
* "Edit" a communication, provided its not completed.
* "Expire" a communication.
* "View Readers" who have read the communication.
![](images/Communication/view-all-communications/view-all-communications-using-actions.png)
