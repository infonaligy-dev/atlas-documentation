#  Claims – View All Claims
  
  
##  Step 1
  
Select "Claims" from the menu under Claims
  
![](images/Claims/view-all-claims/view-all-claims-navigation.png )
  
##  Step 2
  Welcome to the Claims list page, here you will find all your claims.
  There are helpful labels on the page to tell you just how many claims you viewing at a go.
![](images/Claims/view-all-claims/view-all-claims.png )
  
##  Step 3 - Using the search field
 The claims list page provides a robust search field. You may use the field to find specific claims.
![](images/Claims/view-all-claims/view-all-claims-search-by-input.png )
## Step 4 Using filters
### 4.1
The claims list page also provides filters. Filters can be used to narrow down the claims(1).
### 4.2
You may also use the "Clear All" button to reset all filters you have applied(2).

![](images/Claims/view-all-claims/view-all-claims-applying-filters.png )

## Step 5 Claim Actions
Each row in the claims list table provides a three-dots menu on the right side that contains useful actions you can perform on that claim.
You may:  
* Edit a claim
* Print a claim
* Delete a claim

![](images/Claims/view-all-claims/view-all-claims-using-single-action.png )

## Step 5 Bulk Actions
The bulk actions feature allow you to perform the same action on multiple claims at once.
### 5.1
First click the checkbox of the claims you would like to perform a bulk action on (1)

## 5.2 
After you select the claims in  5.1 , you should see the "Bulk Action" dropdown menu show up.Click on it.
Select the bulk action you wish to perform on the claims you have selected.(2). You may
* "Print Selected" claims
* "Delete Selected" claims
![](images/Claims/view-all-claims/view-all-claims-using-bulk-actions.png )

## Step 6 Bulk Actions - Changing Status
The "Change status to" feature is also available to help change the status of multiple claims at a go
## 6.1
First click the checkbox of the claims you would like to change  status for (1)
## 6.2
After you select the claims in  6.1 , you should see the "Change Status To" dropdown menu show up.Click on it.
Select the action you wish to perform on the claims you have selected.(2)
You may change status to:
* Not Submitted
* Submitted
* Received
* Issued
![](images/Claims/view-all-claims/view-all-claims-using-change-status-bulk-actions.png )


## 7 Viewing More Columns
By default the claims list shows you a limited number of important columns containing information for each claim.Viewing the columns list gives you access to other columns hidden by default

## 7.1
First click the columns icon(1).

## 7.2
After Clicking the columns menu will show up. Selecting a column will show it in the claims list and deselecting will hide it accordingly.This can be useful for reducing the columns to show only data useful to you(2).

## 7.3 
You may also click the "Check all" button to show all columns(3).
![](images/Claims/view-all-claims/view-all-claims-viewing-more-columns.png )