#  Users - Adding Users
  Agency admins can only manage agents ie users belonging to their agency.
##  Step 1
  
Select "Add Agent" from the top menu.
  
![](images/users/users-navigating-to-add-agent-as-agency-admin.png)
  
##  Step 2
 Alternatively, you can click the "Create New" button on the agents list page.
![](images/users/users-adding-new-users-navigation.png)


##  Step 3
You will then be taken to the "Add  Agent" page

### 3.1
Fill in the basic information for the agent.(1)

### 3.2
 * Select the role you want the user to have in the "Select Role" field.Refer to the agency roles  documentation for information on roles and features available to them.(2)
 Depending on the role you select, you may see new fields show up or existing ones removed
 * The "Search Agency" field will only contain the agency the Agency Admin belongs to 
 * Select the status of the account being created.
   Statuses that can be set for a user are:   
      1. Pending Approval
      2. Pending Confirmation
      3. Pending Claim
      4. Active
      5. Inactive
### 3.3 
The Companies section only shows for agents and admins. Select the companies you want the agent to rate for if the user being created is an agent.(3)  
 Note: The companies shown available in the list are only companies allowed by the agency.

### 3.4
All permissions are enabled by default. Deselect the ones you wish to not grant the user.(4)

### 3.5
Click the check box in the "Multi-Factor Authentication" section if you want the account to have this enabled by default.(5)

### 3.6
Lastly, Enter a default password for the user.(6)

### 3.7
Click the "Create Agent" button to complete creating your new agent.(7)

![](images/users/users-adding-an-agent-as-agencyAdmin.png)
Congratulations you have successfully created your new user.

  