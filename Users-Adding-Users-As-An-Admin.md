#  Users -Adding Users
  
##  Step 1
  
Select "Users" from the top menu.
  
![](images/users/users-navigating-to-user-list-page.png )
  
##  Step 2
 Click the "Create New" button.
![](images/users/users-adding-new-users-navigation.png)


##  Step 3
After clicking the "Create New" button you will be taken to the "Add New User" page.

### 3.1
Fill in the basic information for the user.(1)

### 3.2
 * Select the role you want the user to have in the "Select Role" field.Refer to the agency roles  documentation for information on roles and features available to them.(2)
 Depending on the role you select, you may see new fields show up or existing ones removed
 * Search for an agency for the agency you would like to assign the user to 
 * Select the status of the account being created.
   Statuses that can be set for a user are:   
      1. Pending Approval
      2. Pending Confirmation
      3. Pending Claim
      4. Active
      5. Inactive
### 3.3 
The Companies section only shows for agents and admins. Select the companies you want the agent to rate for if the user being created is either role.(3)  
 Note: The companies shown available in the list are only companies allowed by the agency.If you do not see a desired company to select,please update the Agency you selected to allow it.(3)

### 3.4
All permissions are enabled by default. Deselect the ones you wish to not grant the user.(4)

### 3.5
Click the check box in the "Multi-Factor Authentication" section if you want the account to have this enabled by default.(5)

### 3.6
Lastly, Enter a default password for the user.(6)

### 3.7
Click the "Create User" button to complete creating your new user.(7)

Congratulations you have successfully created your new user.
![](images/users/users-creating-a-user.png )

  