# Atlas Roles


## Roles Available
* Admin
* SubAdmin
* Agent
* Claims
* Underwriting
* DataEntry
* Adjuster
* AgencyAdmin


## Admin 
The admin manages all users of any role.
There are a number of activities that are only available to admins under the "Admin" menu option.
### Admin Menu
* Adjuster Portal 
The adjuster portal page allows admins to view reports on adjusters.

* Agent Reports 
The agent reports page allows admins to view  reports on agents.

* Agencies
The agencies page shows a list of all agencies.
Admins may:
  - View a Agency  
    1. Admins can view all agents of an agency.
    2. admins can view all subagencies of agency.
    3. View Agency details like agency code.
  - Edit An Agency:  
    1. Admins can add new agents to agencies.
    2. Admins can add sub agencies to agencies.
    3. Edit Agency details like agency code.
  - Delete An Agency

* Agent Applications  
The agent applications page allows admin to view and approve or reject any agent registrations on the website.
 Admins may:     
  - Print an agency application
  - View an agency application
  - Delete an agency application
  - Approve an agency application
  - Reject an agency application  
  
* Companies
The companies page  provides a list of your companies and allows you to easily manage them.  
    Admins may: 
   -  Edit companies
   -  Delete companies

* Communications  
Communications is a powerful communication feature only available for admins, it allows you to send out useful messages, memos and priority alerts to users of the website.Users see these as notifications.

* Counties  
On this page you will find all counties currently allowed on the website.
   Admins may:
   - Edit counties
   - Prohibit binding counties
   - Prohibit binding all counties
   - Delete counties
   - View all counties

* Documents  
Documents is a central hub for all documents uploaded on the website.

* Imports  
Imports page provides features that allow you  to  make bulk additions to mortgages and users.

* Mortgages  
This page contains a list of all mortgages added under the website.
 Admin may:
 - Edit a mortgage
 - Delete a mortgage
 - View all mortgages

* Personal Liability Policy Applications
 Admins have access to personal liability applications.  
 Admins may: 
  - Edit a personal liability application
  - View all personal liability applications
  - Delete a personal liability application

* Settings  
The settings menu page allows you to manage some parts of the website.  
 Admins may:
  - Update top header settings
  - Switch on emergency mode for the website

* Transactions  
Admins can view all records of transactions from bill pay.

* Users  
The users page lists all users on the app.  
 Admins may:  
    - View all users on the website
    - Delete users
    - Ban users
    - Restrict user access
    - Edit users
    - Create a user
### Claims 
* Claims  
Admins have access to all claims filed on the website.
  Admins may: 
  - Create claims
  - View all claims on the website
  - Delete claims
  - Edit claims



* Claims Status Lookup  
Admins may look up the status of any claim.
Claims lookup can be done by the claim number or policy number.

* File a Claim  
Admins may file claims and make updates to claims.

### Policy Menu

* Billing Inquiry  
Admins may make billing inquiries. This will require the company, policy prefix and policy number.

* Bill Pay  
Admins may also initiate the billing process.

* Change Request  
Here admins can view a list of all change requests.  
Admins may:  
- View all change requests on the website
- Edit all change requests on the website
- Delete change requests 
- Print change requests

* Policy Enquiry  
Admins may  also make policy inquiries.This will require the company, policy prefix and policy number.

### Quotes Menu
* Quotes
Admins can also view all quotes created by agents.  
Admins may:  
- Create quotes.
- Update quotes.
- Delete quotes
- View quotes


* PL Application
Admins can also create Personal Liability Applications on atlas. They can also edit and update.


## Agents
There are a number of activities that are only available to agents listed in the menu options.
Agents do not have access to items specified in the "Admins" menu mentioned above

### Claims 
* Claims  
Agents have access to claims **THEY** filed on the website.
  Agents may: 
  - Create claims
  - View claims created by them on the website
  - Delete claims created by them
  - Edit claims created by them



* Claims Status Lookup  
Agents may look up the status of claims.
Claims lookup can be done by the claim number or policy number.

* File a Claim  
Agents may file claims and make updates to claims.

### Policy Menu

* Billing Inquiry  
Agents may make billing inquiries. This will require the company, policy prefix and policy number.

* Bill Pay  
Agents may also initiate the billing process.

* Change Request  
Here as agent can view a list of all change requests **YOU** have created.  
Agents may:  
- View their change requests on the website
- Edit their change requests on the website
- Delete their change requests 
- Print their change requests

* Policy Enquiry  
Agents may also make policy inquiries.This will require the company, policy prefix and policy number.

### Quotes Menu
* Quotes
Agents can also view all the quotes **they** have created.  
Agents may:  
- Create  quotes.
- Update their quotes.
- Delete their quotes
- View their quotes


* PL Application
Agents can also create Personal Liability Applications on atlas. They can also edit and update **their** own applications.




## Agency Admin

The agency admin manages all agents assigned to them.

### Agents
 * Add agents
The agency admin can add new agents to the website. These agents will be manageable by the agency admin.

* Manage Agents
the agency admin can view and manage all agents  under them.
 - Agency admins can ban agents they manage
 - Agency admins can restrict the access of agents they manage
 - Export a list of their agents


* Documents
The agency admin can also see and upload documents on the website



## Underwriters
Underwriters have read access to all agencies they underwrite for.
On the dashboard underwriters can view a list of agencies they underwrite for, their name agency code and current status