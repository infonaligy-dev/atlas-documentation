#  Communications – Create Communication
  
  
##  Step 1

Communications is a useful feature designed to provide an easy and hassle free way to communicate effectively with all users in the application.Note you can only communicate with users within atlas using communications.It currently supports sms, email and in app messaging.

To use communications first click on the "Communications" option under the "Admin" menu.
  
![](images/Communication/create-communication/communication-navigation.png )
  
##  Step 2
Click the "Create New Communication" button to create a new Communication.
![](images/Communication/create-communication/communication-navigation-creating-new-communication.png )

##  Step 3 Creating Communication Draft

You can create your first communication in a few easy steps.

### 3.1
First enter the title of your communication(1).

### 3.2
Fill in the details of your email (2).

### 3.3
Add attachments, if any (3).

### 3.4
Select the priority status of  your email (4).
Priorities you may set are :  
* High
* Medium
* Low

### 3.5
Select additional delivery methods (5).  
Each communication is sent as an in-app-Message by default. This means that all communications sent will show up in the users 'notifications' area within the application. Additional delivery methods available are:  
* SMS
* Email

### 3.6
Search and select the users you wish to send your communication to.
You may send to:
* Specific users you specify using the "Send to individual users" option.
* Specific users by their roles using the  "Send to user roles" option.
* Users belonging to a specific agency by using the "Send to agencies" option.

### 3.7
Click the "Save" button to save your communication.

Congratulations! You have created your first draft communication successfully. Its not over yet. You have to send this communication.
![](images/Communication/create-communication/communication-create-communication-basic.png )
  

## Step 4 Sending Communication

### 4.1
To send your communication, select the "Scheduled" option in the options menu for "Status" (1).

### 4.2
 You should see a "Delivery Date" field show up immediately after selecting the scheduled option (2).
 You may select the time and date you want to send the communication now.

 ### 4.3
 If you wish to send the communication immediately. 
 Click the "Set Delivery Time To Now" option just below the "Delivery Date" field.The minimum time your communication will be sent is 5 minutes (3).

### 4.4
If you are satisfied with your changes. Click the "Save" button to send your communication.

![](images/Communication/create-communication/communication-sending-a-communication.png )

## Step 5 
You have successfully scheduled your first communication to be sent.
You will find information on when your communication will be sent in the status bar that shows soon after.  
You will also notice that your communication has been locked to prevent further updates. To make any changes click on the "Click To Edit" button found in the communication status bar at the top of the screen.  
Note this will cancel the process of sending the communication and you will have to repeat step 4 to resend your communication after making your updates.

![](images/Communication/create-communication/communication-scheduled-communication-notice.png )

## Step 6
### 6.1
Once the your communication is dispatched, you will be redirected to the communications metrics page.
Here you have access to various metrics per user targeted by the communication, clicks, opens, status of the communication(1).

### 6.2
You will also have more insight into just how many of your communications were successfully delivered, bounced and opened(2).

![](images/Communication/create-communication/communication-sent-communication-page.png )
## Step 7 Advanced - tokens
Tokens are a useful feature of communications that allows you to add a personalized touch to all emails.
Rather than "Hello" your emails could say "Hello {users name here}".
### 7.1
To use tokens first select the token to use(1).
### 7.2 
You should notice the token inserted at the last position of the text cursor.

![](images/Communication/create-communication/communication-using-tokens.png )

## Step 8 Advanced - Text formatters

### 8.1
The email body provides robust text formatters to help get your message across. If you have used Microsoft word, then you should be right at home(1).

### 8.2
You'll also notice the status of your communications indicated in the op right.The status may be:  
* draft
* completed
* scheduled

![](images/Communication/create-communication/communication-using-text-formatters.png )