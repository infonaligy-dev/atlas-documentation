#  Notifications – Viewing Notifications
  
  
##  Step 1
After a communication has been sent. It is delivered to all users specified. High priority communications are given the utmost priority by Atlas. The users receiving them will not be allowed to perform any activity on the site until they read and acknowledge that they have received the communication.

 ## 1.1
 To view a high priority notification, click the "View Notifications" button.(1)
![](images/Notifications/notifications-navigation.png )
  
##  Step 2

Welcome to your notifications list page.You will find every communication sent to you as a notification here.

## 2.1
If you wish to return to notifications you may navigate back to this page anytime using the inbox button on the top right of your page.You should also notice the number of unread notifications in your inbox on there.(1)

## 2.2
You may search for specific notifications using the search fields provided. Or filter notifications based on type, priority, and date created using the filters provided.(2)

## 2.3
Each row of the notifications list has a three-dots button that provides access to actions that can be performed on a notification.
Actions that may be performed are: 
* "Read" Notification
Click on the  "Read" Button.

![](images/Notifications/notifications-viewing-notifications-list.png)

##  Step 4

### 4.1
After clicking on the read button the contents of your notification should be opened.

After you have seen the contents of the notification. You may click the checkbox to acknowledge that you have read the contents of the notification. 
![](images/Notifications/notifications-acknowledging-read.png)

##  Step 5

## 5.1
After clicking the checkbox you should see an input field show up.Enter your first and last name to confirm you have read the notification.(1)

## 5.2
You will see the "Submit Acknowledgement" button pop up after entering your first and last name.Note, remember to leave a space between your first and last name.(2)
![](images/Notifications/notifications-acknowledging-read-signing-with-name.png)


## Step 6

### 6.1
Congratulations! You have successfully read your first notification.
![](images/Notifications/notifications-acknowledgement-acknowledged.png)