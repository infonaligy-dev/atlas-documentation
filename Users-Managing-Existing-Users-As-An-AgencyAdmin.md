#  Users - Managing Users As An  Agency Admin
  Agency admins can only see and manage users belonging to their agency
##  Step 1
  To view users belonging to the agency, select "Manage Agents" from the "Agents" menu.
  
![](images/users/users-navigating-to-view-agents-as-agency-admin.png)
  

## Step 2 Banning users using the bulk updates feature
### Step 2.1
Select a number of users to ban using the checkboxes that appear next to each row of the list.(1)

### Step 2.2
 You should see a new dropdown "Bulk Actions" appear on top of the users list view.
 Click this option. This should show a list of actions that can be performed on the selected users.
 Actions that are available are:
 * "Ban selected" users
 * "Restrict selected" users  

### Step 2.3
Select the "Ban Selected" option to ban the users selected.()
![](images/users/users-using-bulk-actions-banning.png)

## Step 3 Restricting users using the bulk updates feature
### Step 3.1
Select a number of users to restrict using the checkboxes that appear next to each row of the list.(1)
### Step 3.2
 You should see a new dropdown "Bulk Actions" appear on top of the users list view.
 Click this option. This should show a list of actions that can be performed on the selected users.
 Actions that are available are:
 * "Ban selected" users
 * "Restrict selected" users  

### Step 3.3
Select the "Restrict Selected" option to restrict the users selected.(3)
This will open the restriction view.
* Enter the level of the restriction.
* Enter the Start Date of the restriction
* Enter the End date of the restriction.

![](images/users/users-using-bulk-actions-restricting.png)
![](images/users/users-restricting-users-via-bulk-updates.png)
