#  Claims – Update A Claim
  
  
##  Step 1 
There are two ways to select a claim to update.

### 1.1
By clicking on the row of the claim selected for update.

![](images/Claims/update-a-claim/update-a-claim-by-row-click-navigation.png )

## 1.2
#### 1.2.1
By clicking on the three-dots button of the claim selected for update (1).
This will show a drop down menu of actions that can be performed
#### 1.2.2
Click "Edit" to update the claim(2).
![](images/Claims/update-a-claim/update-a-claim-using-the-hamburger-menu.png )

##  Step 2
  Welcome to the claims edit screen.
###  2.1
  
The first part of the form is a read only section providing information on the Insured and Agency  and the status of your claim (1).
  
###  2.2
  
After the read only section are you will find all editable fields of your claim.Ensure that all required parts are filled before proceeding (2).
  
###  2.3
  
After making all updates click the "UPDATE" button to save your claim (3)

![](images/Claims/update-a-claim/update-a-claim.png )
## 3 
The claims edit screen also provides a list of secondary actions that can be performed on a single claim.

### 3.1 
First click the "Actions" button (1)
This should show the actions menu.

### 3.2
On the actions menu you may:
 * "Cancel" the updates a claim.
 * "Print" the claim
 * View Sending XML
 * View Receiving XML
![](images/Claims/update-a-claim/update-a-claim-using-secondary-actions.png )