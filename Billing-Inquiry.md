#  Billing Inquiry
  
  
##  Step 1
To make a Billing Inquiry first select the "Billing Inquiry" option under the Policy Menu

![](images/BillingInquiry/billing-inquiry-navigation.png )

## Step 2
###  2.1

Select the company (1).

###  2.2
  
Enter the policy prefix (2).
  
###  2.3
  
Enter the policy number (3)
  
###  2.4
  
After the fields have been filled. Click on the "View Billing Information" button to proceed (4).

###  2.5
  
Should you decide not to proceed. You may click the "Cancel" button (5).

![](images/BillingInquiry/billing-inquiry-making-an-inquiry.png )
  
##  Step 3
Welcome to the billing inquiry page!
You can view the details of the inquiry below.
![](images/BillingInquiry/billing-inquiry.png )

## Step 4
There are a number of actions available for a billing inquiry.

### 4.1
These can be accessed by clicking on the "Actions" dropdown. A menu containing the actions will then be displayed.
You may:
  * "Cancel" to go back
  * "Print" a billing inquiry
  * Proceed to "Pay Bill"
  * View Sending XML (If Admin)
  * View Receiving XML (If Admin)
![](images/BillingInquiry/billing-inquiry-using-actions.png )
