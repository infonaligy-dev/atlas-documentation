#  Users -Managing Users
  
##  Step 1
  
Select "Users" from the top menu.
  
![](images/users/users-navigating-to-user-list-page.png )
  
##  Step 2 - Table Actions
 The users list page provides you with a number of useful actions to perform on users.  
You may: 
  * Upload Banned Agents  
  * Upload Restricted Agents  
  * Upload Users
  * Export Users  

![](images/users/users-using-table-actions.png)

## Step 3 Banning Agents
Click the "Upload Banned Agents" option in Step 2.
This will open an upload modal for you.
Upload a csv in the format specified on the modal to ban affected agents.
Alternatively, you may download a template using the "Download Template" button to get started
![](images/users/users-using-the-banned-agents-feature.png)

## Step 4 Restricting Agents
Click the "Upload Restricted Agents" option in Step 2.
This will open an upload modal for you.
Upload a csv in the format specified on the modal to ban affected agents.
Alternatively, you may download a template using the "Download Template" button to get started
![](images/users/users-using-the-restricted-feature.png)

## Step 5 Upload Users
To add users in bulk, click the "Upload Users" option in Step 2.
This will open an upload modal for you.
Upload a csv in the format specified on the modal to ban affected agents.
Alternatively, you may download a template using the "Download Template" button to get started
![](images/users/users-using-the-banned-agents-feature.png)

## Step 6 Banning users using the bulk updates feature
### Step 6.1
You may alternatively, select a number of users to ban using the checkboxes that appear next to each row of the list.(1)
### Step 6.2
Once you select a number of users, click the "Ban Selected" button to ban the users selected
![](images/users/users-using-bulk-actions-banning.png)

## Step 7 Restricting users using the bulk updates feature
### Step 7.1
You may alternatively, select a number of users to restrict using the checkboxes that appear next to each row of the list.(1)
### Step 7.2
Once you select a number of users, click the "Restrict Selected" button to restrict the users selected.(2)
This will open the restriction view.
* Enter the level of the restriction.
* Enter the Start Date of the restriction
* Enter the End date of the restriction.

![](images/users/users-using-bulk-actions-restricting.png)
![](images/users/users-restricting-users-via-bulk-updates.png)
## Step 8 Exporting Users 
To export the current users showing.Click the "Export Users" option in Step 2.
This will open an export modal for you.
Select the fields you want as headers in the export then click the Export Data
![](images/users/users-using-the-export-feature.png)

## Step 9 Reports
You may also view really useful reports about the users via the actions menu.

![](images/users/users-viewing-report-options.png)
![](images/users/users-the-active-users-page.png)
![](images/users/users-the-user-distribution-page.png)


