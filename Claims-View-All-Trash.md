#  Claims – View All Deleted Claims
  
  
##  Step 1
  
Select "Claims" from the menu under Claims
  
![](images/Claims/view-all-claims/view-all-claims-navigation.png )
  
##  Step 2
  Welcome to the Claims list page, here you will find all your claims.
  There are helpful labels on the page to tell you just how many deleted claims you viewing at a go.
![](images/Claims/view-all-claims/view-all-claims.png )

## Step 3
### 3.1
 Each row of the deleted claims has a hamburger menu containing actions that can be performed on a deleted claim. Click on it(1)

### 3.2
After clicking you will see a menu of actions(2).You may 
 * "Restore" a deleted claim 
 * "Print" a deleted claim
![](images/Claims/view-all-claims/view-all-claims-restoring-trash.png )