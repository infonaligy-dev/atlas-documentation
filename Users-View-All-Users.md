#  User - Viewing All Users
  
##  Step 1
  
Select "Users" from the top menu 
  
![](images/users/users-navigating-to-user-list-page.png )
  
##  Step 2
 Welcome to the users list page. You'll find all users on the website listed here.
 Note: This is only accessible to admins
  
![](images/users/users-viewing-list-of-users.png )
  