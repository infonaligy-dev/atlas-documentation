#  Bill Pay – Using Quick Pay
  
  
##  Step 1
To use Quick Pay first select the "Bill Pay" option under the Policy Menu

![](images/BillPay/bill-pay-navigation.png )

## Step 2
Welcome to the quick pay screen!

### 2.1

Enter the effective date. (1)

###  2.2
  
Enter the policy prefix. (2)
  
###  2.3
  
Enter the policy number. (3)
  
###  2.4
  
After the fields have been filled. Click on the "Next Step" button to continue. (4)

###  2.5
  
Should you decide not to proceed. You may click the "Cancel" button. (5)

## 2.6
Note the helpline at the bottom. Please call if you have any questions or need any help. (6)

![](images/BillPay/bill-pay-using-quick-pay.png )
  
##  Step 3
After clicking "Next Step" you will be brought to the "Verify Policy Info" screen.
### 3.1
Ensure all information displayed in the read-only fields are as expected (1).

### 3.2
Enter the current amount due (2).

### 3.3
Select the payment method (3).

### 3.4
Remember to acknowledge the use of a third party website by clicking the checkbox (4).

You will only be able to proceed if all fields are properly filled out and valid.

### 3.5
After you have finished filling the form, click the "Pay" button to begin the payment process. You may also click the "Cancel" button if you decide not to move forward with payment (5).

### 3.6
If you would like to return to the previous page to make updates, click the "Previous" button (6).
![](images/BillPay/bill-pay-verifying-policy-info.png )


## Step 4
After clicking "Pay" this should open a secure payment pop-up to begin the payment process.

### 4.1
Verify the amount to pay is right then click the "Review" button to continue (1). You may also click the "Cancel" button to discontinue the payments process.

### 4.2 
Click the edit button if you need to make any changes to the payment amount before you submit your payment.
![](images/BillPay/bill-pay-reviewing-payment-amount.png )


## Step 5

### 5.1
If you do click the edit button, you will have the option to select the payment amount.

## 5.2
After selecting an option. Click "OK" to save.
![](images/BillPay/bill-pay-editing-amount.png )


## Step 6 Entering Billing Information - ECheck

### 6.1 
Enter the account details and click "Review". All fields need to be filled properly.

![](images/BillPay/bill-pay-entering-billing-information-account.png )

## Step 6 Entering Billing Information - Credit Card

### 6.1 
Enter the credit card details and click "Review". All fields need to be filled properly.

### 6.2
Ensure the credit card type is supported (2).

![](images/BillPay/bill-pay-entering-billing-information-credit-card.png )

## Step 7
### 7.1 
Verify the payment account details you have provided is correct and the amount to be paid is right (1).

### 7.2
After verification, click the "Pay" button to initiate payment when ready.

![](images/BillPay/bill-pay-verifying-amount-and-paying-bill.png )

## Step 8
Congratulations! You have successfully completed a payment.

Remember to back up a copy of the receipt via the options provided. You may backup by:
 * Printing a receipt using the "Print Receipt" button
 * Downloading a receipt using the "Download Receipt" button

![](images/BillPay/bill-pay-after-payment-actions.png )