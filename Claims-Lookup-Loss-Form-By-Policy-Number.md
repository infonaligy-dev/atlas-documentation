#  Claims – Lookup Claim By Policy Number
  
  
##  Step 1
  
Select "Claims Status Lookup" from the menu under Claims
  
![](images/Claims/lookup-a-claim/lookup-claim.png )
  
##  Step 2
  
###  2.1
  
Make sure you have the "Policy Number" option selected (1).
  
###  2.2
  
Select the policy prefix (2).
  
###  2.3
Enter the policy number (3).

### 2.4
Enter 
After you enter the policy number, click "Lookup Claim" button to lookup the claim (3).
  
![](images/Claims/lookup-a-claim/lookup-claim-by-policy-number.png )
  
##  Step 3

### 3.1 
View the results of your claim lookup.
This will be a list of claims filed under the policy number.

### 3.2 
You can click the view button to view a claim.
You are done! 
![](images/Claims/lookup-a-claim/lookup-claim-by-policy-number-result.png )
  